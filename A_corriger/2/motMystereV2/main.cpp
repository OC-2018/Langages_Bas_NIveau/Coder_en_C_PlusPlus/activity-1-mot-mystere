#include <iostream>
#include <fstream> // Utiliser les fichiers
#include <string>
#include <vector> // Utiliser les vectors
#include <ctime> // Utiliser l'al�atoire
#include <cstdlib> // Utiliser les commandes syst�me

using namespace std;

/* Le fichier "dico.txt" doit �tre � la m�me racine que l'ex�cutable ou du projet,
il est modifiable en cours de partie mais il ne doit pas �tre vide sinon un message
d'erreur appara�tra. */

string melangeMot(string motChoisi) // Fonction ayant pour r�le de m�langer le mot myst�re
{
    system("cls");
    unsigned int nbAleatoire;
    string motMelange,motChoisiCopie(motChoisi); // On cr�e une copie du mot myst�re afin de pouvoir �viter le m�lange de la m�me lettre
    for (unsigned int i(1);i<=motChoisiCopie.size();i++) // M�lange du mot
    {
        nbAleatoire=rand()%motChoisi.size();
        motMelange+=motChoisi[nbAleatoire];
        motChoisi.erase(nbAleatoire,1);
    }
    return motMelange; // Le r�sultat est le mot m�lang�
}

void modeSolo() // D�marrage du mode solo
{
    string rejouer,motChoisi,motMelange,tentative,ligne; // D�claration de toutes les variables utilis�es
    do
    {
        system("cls");
        cout << "Bienvenue dans le mode solo ! Tentez de retrouver le mot melange par le\nprogramme en moins de 3 tentatives !\n\n";
        ifstream dico("dico.txt"); // On sp�cifie le chemin du fichier "dico.txt"
        if (!dico) // Si le fichier n'existe pas ou s'il n'est pas � la m�me racine du programme
        {
            cout << "Impossible de commencer une partie.\nRAISON : Fichier \"dico.txt\" manquant a la racine de l'executable ou du projet.\n\nVous allez retourner au menu.\n\n";
            system("pause");
            system("cls");
        } // On passe le jeu et on retourne au menu
        else // Si le fichier "dico.txt" existe bien
        {
            cout << "Le fichier \"dico.txt\" a ete trouve. La partie va donc demarrer.\n\n";
            system("pause");
            unsigned int ligneChoisie;
            vector<string> motLigne; // Cr�ation d'un vector qui contient dans chaque case un mot du fichier
            while(getline(dico,ligne)) // Boucle ajoutant chaque mot du fichier dans des cases du vector
            {
                motLigne.push_back(ligne);
                ligne="NONE";
            }
            ligneChoisie=rand()%motLigne.size(); // Choix d'une case al�atoire du vector
            motChoisi=motLigne[ligneChoisie]; // On r�cup�re la valeur de cette case
            motMelange=melangeMot(motChoisi); // On m�lange le mot myst�re
            cout << "Le mot mystere a ete choisi. A vous de le retrouver !\n";
            unsigned int essais(3);
            do
            {
                cout << "\nQuel mot se cache derriere " << motMelange << " ?\n";
                cin >> tentative;
                essais--; // Utilisation d'1 essai
                if (tentative!=motChoisi) // Si l'utilisateur se trompe
                {
                    cout << "\nCe n'est pas le bon mot !\nIl vous reste " << essais << " tentative(s).\n";
                }
            } while (tentative!=motChoisi && essais>0); // Tant que l'utilisateur se trompe et qu'il a encore des essais
            if (essais==0 && tentative!=motChoisi) // Si l'utilisateur n'a plus d'essai
            {
                cout << "\nDommage ! Vous n'avez pas reussi a retrouver le mot.\nLe mot mystere etait \"" << motChoisi << "\".\n" << endl;
            }
            else // Si l'utilisateur a retrouv� le mot myst�re
            {
                cout << "\nBravo ! Vous avez retrouve le mot mystere \"" << motChoisi << "\" !\n\n";
            }
            cout << "Voulez-vous rejouer (O/N) ?\n";
            cin >> rejouer;
            cout << endl;
            system("pause");
            system("cls");
        }
    } while (rejouer=="o" || rejouer=="O"); // Tant que l'utilisateur souhaite jouer
} // Retour au menu

void modeMulti() // D�marrage du mode 2 joueurs
{
    string rejouer,motChoisi,motMelange,tentative; // D�claration de toutes les variables utilis�es
    do
    {
        system("cls");
        cout << "Bienvenue dans le mode 2 joueurs ! Chaque joueur a un role different : L'un\nchoisit un mot a faire deviner a l'autre qui doit le retrouver en moins de 3\ntentatives.\n\nLe premier joueur doit choisir un mot qu'il doit faire deviner :\nLe mot mystere est ";
        cin >> motChoisi; // R�cup�ration du mot myst�re
        motMelange=melangeMot(motChoisi); // M�lange du mot myst�re
        cout << "Votre adversaire a choisi le mot mystere. A vous de le retrouver !\n";
        unsigned int essais(3); // D�claration de la variable autorisant 3 essais
        do
        {
            cout << "\nQuel mot se cache derriere " << motMelange << " ?\n";
            cin >> tentative; // R�cup�ration de la tentative
            essais--; // Utilisation d'1 essai
            if (tentative!=motChoisi) // Si l'utilisateur se trompe
            {
                cout << "\nCe n'est pas le bon mot !\nIl vous reste " << essais << " tentative(s).\n";
            }
        } while (tentative!=motChoisi && essais>0); // Tant que l'utilisateur se trompe et qu'il a encore des essais
        if (essais==0 && tentative!=motChoisi) // Si l'utilisateur n'a plus d'essai
        {
            cout << "\nDommage ! Vous n'avez pas reussi a retrouver le mot.\nLe mot mystere etait \"" << motChoisi << "\".\n\n";
        }
        else // Si l'utilisateur a retrouv� le mot myst�re
        {
            cout << "\nBravo ! Vous avez retrouve le mot mystere \"" << motChoisi << "\" !\n\n";
        }
        cout << "Voulez-vous rejouer (O/N) ?\n";
        cin >> rejouer;
        cout << endl;
        system("pause");
        system("cls");
    } while (rejouer=="o" || rejouer=="O"); // Tant que l'utilisateur souhaite jouer
} // Retour au menu

int main() // D�but du programme
{
    srand(time(0)); // Initialisation de l'al�atoire
    unsigned int optionMenu; // D�claration de la variable servant � la s�lection du mode de jeu
    do
    {
        cout << "Mot Mystere V2 - Cree par M4t13u a l'aide du cours d'OpenClassrooms.\n\nVous voici au menu du programme. Que voulez-vous faire ?\n1: Jouer au mode solo\n2: Jouer au mode 2 joueurs\n"; // Menu du programme
        cin >> optionMenu;
        cout << endl;
        switch(optionMenu)
        {
        case 1: // Si l'utilisateur souhaite jouer seul
            cout << "Vous allez jouer au mode solo.\n\n";
            system("pause");
            modeSolo(); // D�marrage du mode solo
            break;
        case 2: // Si l'utilisateur souhaite jouer avec un ami
            cout << "Vous allez jouer au mode 2 joueurs.\n\n";
            system("pause");
            modeMulti(); // D�marrage du mode 2 joueurs
            break;
        default: // Si l'utilisateur a entr� une mauvaise valeur
            cout << "Veuillez choisir une option valide.\n\n";
            system("pause");
            system("cls");
            break;
        }
    } while (optionMenu!=1,2); // Tant que la saisie de l'utilisateur est incorrecte
    return 0;
}
