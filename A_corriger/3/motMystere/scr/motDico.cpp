/*
 * motDico.cpp
 *
 *  Created on: 21 oct. 2018
 *      Author: moroo
 */

#include "../header/motDico.h"

int nombreMots(){
	int nbMots(0);

	std::string   ligne;
	std::string   dico("dico.txt");
	std::ifstream monFlux(dico.c_str());

	if(monFlux){
		while(std::getline(monFlux, ligne)){
			++nbMots;
		}
		//std::cout << "Il y a : " << nbMots << " mots." << std::endl;
	}
	else {
		std::cout << "ERREUR: Impossible d'ouvrir le fichier." << std::endl;
	}

	return nbMots;
}

std::string piocheMot(int nbMots){
	std::string   mot, ligne;
	std::string   dico("dico.txt");
	std::ifstream monFlux(dico.c_str());

	int indexMot = rand() % nbMots;
	//std::cout << "indexMot : " << indexMot << std::endl;

	if(monFlux){
		unsigned int i = 0;

		while(std::getline(monFlux, ligne)){
			if (i == indexMot-1){
				mot = ligne;
				//std::cout << "i : " << i << std::endl;
				//std::cout << "Le mot pioch� est : " << mot << std::endl;
			}
			++i;
		}

	}
	else {
		std::cout << "ERREUR: Impossible d'ouvrir le fichier." << std::endl;
	}


	return mot;
}

