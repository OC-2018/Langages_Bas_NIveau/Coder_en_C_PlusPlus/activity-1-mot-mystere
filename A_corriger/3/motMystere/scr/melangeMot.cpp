/*
 * melangeMot.cpp
 *
 *  Created on: 21 oct. 2018
 *      Author: moroo
 */

#include "../header/melangeMot.h"

std::string melangeMot(std::string mot){
	std::string melange(""), motMelange(mot);

	unsigned int compteur;
    for (compteur = 0; compteur<mot.size(); compteur++){
        int position = rand() % motMelange.size();
        melange += motMelange[position];
        //std::cout << "Lettre au hasard : " << motMelange[position] << std::endl;
        motMelange.erase(position,1);
     }
     //std::cout << "Le mot melang� est : " << melange << "." << std::endl;

	return melange;
}
