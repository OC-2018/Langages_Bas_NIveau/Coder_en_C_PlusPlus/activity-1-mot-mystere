

/*
 * File:   mot_myster.cpp
 * Author: Medyassen Oukil
 *
 * Created on 18 avril 2018, 21:43
 */
#include <iostream>
#include <cstdlib>
#include <string>
#include <ctime>
#include <ctype.h>
#include <fstream>
#include "header.h"
/*
*
*
*
*
*
*/
using namespace std;


int main(int argc, char** argv)
{
    //D�claration de toutes les variables n�cessaires au programme :
    //*****************************************************************

    string motOriginal  ; //Le  mot saisi par le 1er joueur sera stock� ici
    string motDesordone ;//Mot en desordre (d�sordonn� par l'ordinateur) sera stock� ici
    string motSaisi ;//Tout les mot propos�s par le 2eme joueur seront stck�s ici
    int points ;//Initialiser les points quand la partie commence au nombre maximum qui vaut 20
    string rejouerLaPartie ;
    bool modeDeJeu ;

    /*-----------------------------------Contenu du jeu r�peter le jeu tant que le joueur veut rejouer -------------------------------------*/

    do
    {
        initialiserLeJeu (motDesordone , motOriginal , motSaisi,rejouerLaPartie,points);//Initialiser tyout le jeu
        interface(SEPARER);
        interface (DEBUT) ; //Afficher l'interface de d�but avec les explication sur le principe du jeu
        cin>>modeDeJeu;//S�l�ction du mode de jeu
        //---------------------------------------------------
        if ( modeDeJeu== MODE_H_VS_H){
            cout<<"C'est au premier joueur d'introduire secretement son mot choisi..."<<endl<<endl;
            cin>>motOriginal; //Saisi du mot original par le 1er joueur
            mettreEnMajuscule(motOriginal); //Mis en majuscule du mot par r�f�rence.string rejouerLaPartieMode_1 ;
            motDesordone = desordoner (motOriginal);//Mettre en d�sordre les lettres du mot original et les stocker dans la variable �quivalente


            interface (SEPARER) ; //Effacer la console ( ou Ajouter du vide pour que le 2�me joueur ne puisse pas voir le mot original)
            interface(VOS_POINTS_H_V_H);//Afficher l'interface de commencement de la partie au 2�me joueur

            cout <<motDesordone<<endl ;//Afficher le mot En d�sordre au 2�me joueur
            cin >>motSaisi ;
            mettreEnMajuscule(motSaisi);

            while ((motSaisi != motOriginal) && (points !=0))
            {
                points -- ;
                interface(SEPARER); //Effacer l'�cran pour ne pas encombrer la console.
                cout <<"Non, ce n'est pas le mot. Essayer encore une fois il vous reste : "<<points<<" points /20 :"<<endl;
                motSaisi = "" ;
                cout <<"Revoici le mot en d�sordre : "<<motDesordone<<endl;
                cin >>motSaisi ;
                mettreEnMajuscule(motSaisi);
            }

            if (motSaisi == motOriginal)
            {

                cout <<"bravo! C'est le bon mot Votre score est  "<<points <<"/20"<<endl;

            }


            if (points==0)
            {
                cout <<"Vous avez perdu il ne vous reste plus aucun point."<<endl;
            }

            cout<<"Voules vous rejouer ? Saisissez \"oui\" ou  \"non\""<<endl;
            cin >>rejouerLaPartie ;
            mettreEnMajuscule(rejouerLaPartie);
        }
        //---------------------------------------------------
        else if (modeDeJeu == MODE_ORD_VS_H){
             //*Initialiser le jeu pour permettre aisni de
            //v�rifier si le mot � �t� tir� avec succ�s du dictionnaire ou si une erreure c'est produite et pour ainsi faire
            //on peut juste v�rifier si le mot tir� est vide ou non si non alors le mot a �t� tir� avec succ�s.*//
            initialiserLeJeu(motOriginal,motDesordone,motSaisi,rejouerLaPartie,points);


            interface (SEPARER) ; //Effacer la console ( ou Ajouter du vide pour que le 2�me joueur ne puisse pas voir le mot original)
            motOriginal = tirerUnMot(); //TirerUn mot du dictionnaire.
            if (motOriginal != ""){
                mettreEnMajuscule(motOriginal);
                motDesordone = desordoner(motOriginal);
                interface(VOS_POINTS_H_V_O);//Afficher l'interface de commencement de la partie au joueur.

                cout<<motDesordone<<endl; //Afficher le mot en d�sordre.

                cin>>motSaisi; //Demande de saisie du mot par le joueur.
                mettreEnMajuscule(motSaisi);
                while  ((motSaisi != motOriginal)&&(points!=0)){
                        points -- ;
                        interface(SEPARER); //Effacer l'�cran pour ne pas encombrer la console.
                        cout <<"Non, ce n'est pas le mot. Essayer encore une fois il vous reste : "<<points<<" points /20 :"<<endl;
                        motSaisi = "" ;
                        cout <<"Revoici le mot en desordre : "<<motDesordone<<endl;
                        cin >>motSaisi ;
                        mettreEnMajuscule(motSaisi) ;
                    }

                if (motSaisi == motOriginal){
                    cout <<"bravo! C'est le bon mot Votre score est  "<<points <<"/20"<<endl;
                }
                else if (points == 0 ) {
                        cout<<"Vous avez perdu il ne vous reste plus aucun point."<<endl;
                }
            }
            }



    } while (rejouerLaPartie == "OUI");


    return 0;
}


//*******************************************************//
//        Fonction qui g�re les interfaces du jeu        //
//        et l'interface afficher d�pend de 3 valeurs    //
//        possibles d'un seul param�tre obligatoir       //
//                                                       //
//*******************************************************//
void interface(const int val)
{
    if (val == DEBUT)
    {
        cout <<"                          ***********************************"<<endl ;
        cout <<"                          ****M*O*T***M*Y*S*T*E*R************"<<endl ;
        cout <<"                          ***********************************"<<endl<<endl<<endl ;
        cout <<"Voici le principe du jeu:"<<endl;
        cout <<"le jeu se joue a deux ou contre l'ordinateur."<<endl<<endl;
        cout <<"********MODE (1) : Mode deux joueur*******"<<endl
             <<"un premier joueur saisi un mot de son choix, en suite l'ordinateur"<<endl;
        cout <<"prend les lettres du mot et genere un mot en desordre "<<endl
             <<"avec ses lettres et c'est au deuxieme joueur "<<endl
             <<"de deviner le mot original en moins de 20 points."<<endl<<endl;
        cout <<"********MODE (2) : Mode contre l'ordinateur*******"<<endl;
        cout <<"L'odinateur prend un mot au hazard de sa base de donnee et"<<endl
            <<" met en desordre ce mot et c'est a vous de le deviner"<<endl<<endl;
        cout <<"saisissez 0 pour le MODE(1) et 1 pour le MODE(2) ...Allez y."<<endl<<endl;
    }
    else if (val == SEPARER)
    {
        for (int i =0 ;i<50 ;i++){
            cout<<endl;
        }
    }
    else if (val == VOS_POINTS_H_V_H)
    {
        cout<<"Maintenant c'est au joueur 2 de jouer Vous avez "<<POINTS_MAX<<" points pour trouver le mot original ";
        cout<<"Le mot en desordre sera afficher en bas. "<<endl<<"...Allez y :"<<endl ;
    }
    else if (val == VOS_POINTS_H_V_O)
    {
        cout<<"Maintenant c'est a vous de jouer Vous avez "<<POINTS_MAX<<" points pour trouver le mot original ";
        cout<<"Le mot en desordre sera afficher en bas. "<<endl<<"...Allez y :"<<endl ;
    }
}

//*******************************************************//
//        Fonction qui met en d�sordre les lettres       //
//                        du mot original                //
//*******************************************************//
string desordoner (string val)
{
    srand(time(0));

    string  copieDuMotOriginal("");
    string motRetour ("");


    int position(0);

    copieDuMotOriginal = val;

    while(copieDuMotOriginal.size ()!= 0)
    {
        position  = rand()% copieDuMotOriginal.size ();
        motRetour+=copieDuMotOriginal[position];
        copieDuMotOriginal.erase (position,1);
    }
    mettreEnMajuscule (motRetour);
    return motRetour ;
}


//*******************************************************//
//        Fonction qui transforme n'importe quelle       //
//        chaine de caract�res en majusscule             //
//*******************************************************//


void mettreEnMajuscule (string &val)
{

    for (unsigned int i = 0 ; i < val.size () ; i++)
    {
        val[i] = std::toupper(val[i]);
    }
}

//*******************************************************//
//        Fonction qui tire un mot au hasard             //
//        d'un fichier                                   //
//*******************************************************//

string tirerUnMot (){
    int posCurseur (0);
    ifstream dico ("dico.txt");
    string motChoisi ;

    srand(time(0));
    posCurseur =rand()%256 ;

    if (dico){

         for  (int i = 0 ; i!= posCurseur ;i++){
            motChoisi ="";
            getline(dico,motChoisi);
         }
         return motChoisi ;

    }else cout<< "UNE ERREUR C'EST PRODUITE LORS DE L'OUVERTURE DU FICHIER"<<endl;


}

//*******************************************************//
//        Fonction qui initialise toutes les             //
//        varibles n�cessaire au bon fonctionnement      //
//        du programme � leurs valeurs ad�quates         //
//*******************************************************//


void initialiserLeJeu (string &val_1,string &val_2,string &val_3,string &val_4,int &val_5) {
    val_1 = "" ;
    val_2 = "" ;
    val_3 = "" ;
    val_4 = "" ;
    val_5 = POINTS_MAX ;
}



