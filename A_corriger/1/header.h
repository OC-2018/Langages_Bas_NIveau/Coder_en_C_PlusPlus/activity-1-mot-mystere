#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

#define DEBUT 0
#define SEPARER 1
#define VOS_POINTS_H_V_H 2
#define VOS_POINTS_H_V_O 3
#define POINTS_MAX 20
#define MODE_H_VS_H 0
#define MODE_ORD_VS_H 1
#define REJOUER OUI

#endif // HEADER_H_INCLUDED

void interface(const int );

std::string desordoner (std::string );

void mettreEnMajuscule (std::string &);

std::string tirerUnMot ();

void initialiserLeJeu (std::string & , std::string & , std::string & , std::string & , int &);
