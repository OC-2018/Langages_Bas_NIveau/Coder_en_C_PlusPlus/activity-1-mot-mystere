#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <vector>
#include <fstream>

using namespace std;


string selectionMot()
{
    string motMystere;
    string fichierDico("dico.txt");
    int tailleDico;
    int position;

    ifstream monDico(fichierDico.c_str());

    if (monDico)
    {
        int x;
        x = rand();
        monDico.seekg(0, ios::end);
        tailleDico = monDico.tellg();
        position = x%100;
        position = tailleDico*position/100;
        monDico.seekg(position, ios::beg);
        monDico.ignore();
        // Lecture de la ligne courante partielle
        getline(monDico,motMystere);
        // Lecture du mot de la ligne suivante
        getline(monDico,motMystere);
    }
    else
    {
        cout << "Erreur de lecture du fichier: " << fichierDico << endl;
    }
    return motMystere;
}

string melangerLettres(string mot)
{
    string motMelange;
    int lgMot(0);
    int position(0);

    lgMot = mot.size();
    while (mot.size() != 0)
    {
        lgMot = mot.size();
        position = rand()%lgMot;
        motMelange.push_back(mot[position]);
        mot.erase(position,1);
    }
    return motMelange;
}
int main()
{
    // Saisie du mot mystere

    string motATrouver, motMelange, motReponse;
    string reponseON;
    int essaisMax(5);
    unsigned int essaisFaits;
    bool continuer(true);
    do
    {
        srand(time(NULL));
        essaisFaits = 0;
        cout << "Voulez vous laisser le systeme choisir (O/N)" << endl;
        cin >> reponseON;
        if (reponseON == "O" || reponseON == "o")
        {
            motATrouver = selectionMot();
        }
        else
        {
            cout << "Entrez le mot mystere:" << endl;
            cin >> motATrouver;
        }

        motMelange = melangerLettres(motATrouver);

        do
        {
        cout << "Mot mystere = " << motMelange << endl;
        cout << "Entrez votre proposition: " << endl;
        cin >> motReponse;
        essaisFaits++;
            if (motReponse == motATrouver)
            {
              cout << "Bravo !!" << endl;
            }
            else
            {
                if (essaisFaits < essaisMax)
                {
                    cout << "Dommage recommencez !!" << endl;
                }
                else
                {
                    cout << "Le nombre de " << essaisMax << " essais maximum est atteint !!" << endl;
                    cout << "Le mot mystere etait: " << motATrouver << endl;
                    break;
                }
            }

        }while(motReponse != motATrouver);

        cout << "Voulez vous refaire une partie (O,N)? " << endl;
        cin >> reponseON;
        if (reponseON == "O" || reponseON == "o")
        {
            continuer = true;
        }
        else
        {
            continuer = false;
        }

    }while(continuer);

return 0;
}
