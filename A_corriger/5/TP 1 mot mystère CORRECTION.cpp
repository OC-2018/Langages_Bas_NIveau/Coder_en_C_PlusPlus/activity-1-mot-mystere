#include<iostream>
#include<string>
#include<ctime>
#include<cstdlib>
#include<vector>
#include<fstream>

/*Dans ce programme nous allons crée un jeu ou le joueur doit trouver le mot mystére
    - Le mot mystère sera déterminé aléatoirement à partir d'un fichier dico.txt
    - Le joueur à 5 tentatives pour trouver le bon mot 
*/

std::string melange(std::string motJoueur)
{
    int valeur = 0;
    std::string motMelange;

    while(motJoueur.size() != 0)
    {
        valeur = rand() % motJoueur.size();
        motMelange += motJoueur[valeur];
        motJoueur.erase(valeur, 1);
    }

    return motMelange;
}


int main()
{
    std::locale::global(std::locale("")); //Permet d'afficher les accents 

    //Défintion des variables
    std::string ligne, motMajuscule, motReponse, motMelange, motFixe, continuerJeu = "O",pseudoJ;
    std::srand((unsigned int)time(0));   //Pour éviter que le programme renvoi la méme valeur au méme moment
    int k = 0, compteur = 0;
    std::vector<int> scoreJ;            //Stockage du score des deux joueurs scoreJ[i] avec i:joueur
    double moyenne(0);

    std::cout << "Bienvenue sur le jeu du mot mystère !!" << std::endl;
    std::cout << "Entrez votre pseudo de joueur : ";
    std::cin >> pseudoJ;
    std::cout << "\n";

    do
    {
        //Compteur du nombre de partie
        compteur++;


        //Saisie du mot aléatoire dans une bibliothèque annexe 
        std::string const nomFichier = "C:/Users/Ecth2570/Documents/C++/test/dev/TP 1/dico.txt";
        std::ifstream monFlux(nomFichier.c_str());   //Ouverture du fichier avec les mots 
        monFlux.seekg(0, std::ios::end);            //On se déplace à la fn du fichier pour récupérer la taille

        int tailleFichier, positionDico;
        tailleFichier = monFlux.tellg();           //On récupère la taille du fichier par rapport à la position

        if(monFlux)
        {
            positionDico = (rand() % tailleFichier) + 1 ;        //Renvoi un nombre aléatoire par rapport à la taille du fichier
            monFlux.seekg(0,std::ios::beg);                          
            
            for(int i = 0; i < positionDico; i++)
            {
            getline(monFlux, ligne);
            }
            
            motFixe = ligne;
            
            std::cout << ligne << std::endl;
            std::cout << positionDico << std::endl;
            std::cout << tailleFichier << std::endl;
        }
        else{
            std::cout << "ERREUR: Ouverture du document impossible." << std::endl;
        }

        //Création de saut de ligne pour cacher le mot au J2
        for(int i = 0; i < 50; i++)
        {
            std::cout << "\n" << std::endl;
        }

        //Génération du mots mélangé par l'ordinateur
        while(motFixe != motReponse)
        {
            motMelange = melange(ligne); //Appel de la fonction pour le melange des lettres

            //Le joueur saisie sa réponse.
            std::cout << pseudoJ << " ! Devinez ce mot : " << motMelange << std::endl;
            std::cin >> motReponse;

            //Condition de réussite ou échec
            if(motFixe == motReponse)
            {
                std::cout << "Vous avez gagné en " << k+1 << " tour(s) !" << "Bravo !!" << "\n" << std::endl;
                scoreJ.push_back(k+1);
                k = 0;  
                std::cout << "Voulez vous faire une nouvelle partie? (O/N)" << std::endl;
                std::cin >> continuerJeu;
            }
        
            else
            {
                std::cout << "Ce n'est pas le bon mot ! Try again noob" << "\n" << std::endl;
                std::cout << "Il vous reste " << 4-(k) << " essaies." << std::endl;
                k++;
            }
        
            if(k == 4)
            {
                std::cout << "\n";
                std::cout << "Vous avez utilisé vos 5 coups ! C'est terminé !" << std::endl;
                motReponse = motFixe;
                std::cout << "Le mot du caché était : " << motFixe << std::endl;
                std::cout << "\n";
                scoreJ.push_back(5); 
                std::cout << "Voulez vous faire une nouvelle partie? (O/N)" << std::endl;
                std::cin >> continuerJeu;
                k=0; 
            }
            
        }

    } while (continuerJeu == "O" || continuerJeu == "o");

    //Calcul de la moyenne du joueur
    for(int i = 0; i < scoreJ.size(); i++) //On utilise "scoreJ.size()" pour limiter le nombre de boucle en fonction de la taille du tableau.
    {
        moyenne += scoreJ[i];
    }

    moyenne /= scoreJ.size();
    std::cout << "La moyenne de " << pseudoJ << " est : " << moyenne << " coup(s)" << std::endl;

    std::cout << "Merci d'avoir joué !" << std::endl;

    system("pause");

    return 0;

}
