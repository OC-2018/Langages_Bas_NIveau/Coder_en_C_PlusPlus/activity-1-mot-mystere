#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <ctime>
#include <cstdlib>
#include <vector>

using namespace std;

string melangerLettres(string mot)
{
    string melange;
    int position, indexCarac ;

    while (mot.size() != 0)                 // Tant qu'il reste des lettre dans le mot
    {
        indexCarac = rand() % mot.size();   // On choisi un numero de lettre auhasard dans le mot
        melange += mot[indexCarac];         // AJoute la lettre pris au hasard dans la variable "melange"
        mot.erase(indexCarac, 1);           // On retire la lettre selectionne du mot mysere
    }

    return melange;
}

/** \brief  Choisi un mot parmit une liste de mots inscrit dans un fichier
 *
 * \return motAleatoire: tableau[nombre]
 *
 */
string choixMot()
{
    string ligne, motMystere;
    int nombre(0);
    vector<string> tableau(0);

    ifstream fluxDico("dico.txt");

    if(fluxDico)
    {
        fluxDico.seekg(0, ios::beg);          // On place le curseur en d�but de fichier

        while(getline(fluxDico, ligne)) {    // Lit et ajoute chaque ligne dans un tableau afin de faciliter la d�lection aleatoire de la ligne
           tableau.push_back(ligne);
        }

        nombre = rand() % tableau.size();    // G�n�re un nombre al�atoire entre 0 et le nombre d'�l�ments du tableau

        return tableau[nombre];              // Retourne le mot selectionner aleatoirement
    }
    else                                    // SI l'ouverture du fichier n'a pas fonctionner
    {
        std::cout << "ERREUR: Impossible d'ouvrir le fichier !!! " << std::endl;
        return 0;
    }
}

int main()
{
    srand(time(0)); // Initialisation des nombres aleatoire
    char jouer('y');
    int chanceMax(5), essaiJoueur(0);

    do {
        string motMystere, motMelanger, userChoice;

        essaiJoueur = 0; // Remise a z�ro des essais du joueur

        //-------------------------------
        // 1 --- CHOIX DU MOT MYSTERE
        //-------------------------------
        motMystere = choixMot();

        //-------------------------------------------------------------
        // 2 --- RECUPERATION DU MOT MELANGER A PARTIR DU MOT MYSTERE
        //-------------------------------------------------------------
        motMelanger =  melangerLettres(motMystere);

        //-------------------------------------------------------------
        // 3 --- ON DEMANDE A L'UTLISATEUR LA REPONSE
        //-------------------------------------------------------------
        do {
            std::cout << "||=============================================================================================================||" << std::endl;
            std::cout << "||-----------------------------> Quel est ce mot ? ===> " << motMelanger << "<---------------------------------||" << std::endl;
            std::cout << "||=============================================================================================================||" << std::endl;
            cin >> userChoice;

            if (userChoice == motMystere) {
                std::cout << "||=========================================================================================================||" << std::endl;
                std::cout << "||-------------------------------> FELICITATION !! VOUS AVEZ GAGNER !! <-----------------------------------||" << std::endl;
                std::cout << "||=========================================================================================================||" << std::endl;
            } else {
                essaiJoueur += 1;   // Incremntation du compteur d'essai du joueur
                std::cout << "||=========================================================================================================||" << endl;
                std::cout << "||--------------> Desole, mais le mot entrer ne correspond pas au mot mystere !!! <------------------------||" << endl;
                std::cout << "||---------------------------------------------------------------------------------------------------------||" << endl;
                std::cout << "||----------> Attention il ne vous reste plus que : " << chanceMax - essaiJoueur << " essai(s) <-----------||" << endl;
                std::cout << "||=========================================================================================================||" << endl;
            }

        } while (userChoice != motMystere && essaiJoueur != chanceMax);

        if ( essaiJoueur == chanceMax)
        {
            std::cout << "||===================================================================================================||" << endl;
            std::cout << "||----------> Pas de chance, vous avez depasser les 5 essais qui vous etait accorde !! <-------------||" << endl;
            std::cout << "||---------------------------------------------------------------------------------------------------||" << endl;
            std::cout << "||----------> Le mot mystere etait : " << motMystere << "<-------------------------------------------||" << endl;
            std::cout << "||===================================================================================================||" << endl;
        }

        std::cout << "||=============================================||" << endl;
        std::cout << "||-------> Voulez vous rejouer ? (y/n) <-------||" << endl;
        std::cout << "||=============================================||" << endl;
        std::cin >> jouer;

        if (jouer == 'n') {
            std::cout << "||====================================================================================================||" << endl;
            std::cout << "||---> Toutes l'equipe vous remercie de votre visite. A tres bientot pour retentez votre chance !!----||" << endl;
            std::cout << "||====================================================================================================||" << endl;
        }


    } while(jouer == 'y');



    // FIN DU PROGRAMME
    return 0;
}

